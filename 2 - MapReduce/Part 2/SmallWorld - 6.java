/*
 *
 * CS61C Spring 2013 Project 2: Small World
 *
 * Partner 1 Name: Wesley To
 * Partner 1 Login: cs61c-cv
 *
 * Partner 2 Name: Jonathan Ma
 * Partner 2 Login: cs61c-dq
 *
 * REMINDERS: 
 *
 * 1) YOU MUST COMPLETE THIS PROJECT WITH A PARTNER.
 * 
 * 2) DO NOT SHARE CODE WITH ANYONE EXCEPT YOUR PARTNER.
 * EVEN FOR DEBUGGING. THIS MEANS YOU.
 *
 */

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.Math;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class SmallWorld {
	// Maximum depth for any breadth-first search
	public static final int MAX_ITERATIONS = 20;

	public static class Quintet implements Writable {

		public long start;
		public int distance = 0;
		public int state = 0;
		public ArrayList<LongWritable> neighbors = new ArrayList<LongWritable>();

	    public Quintet(long st, int d, int s, ArrayList<LongWritable> n) {
			this.start = st;
			this.distance = d;
			this.state = s;
			this.neighbors = n;
		}
		
		public Quintet() {
			// does nothing
		}

		// Serializes object - needed for Writable
		public void write(DataOutput out) throws IOException {
			out.writeLong(start);
			out.writeInt(distance);
			out.writeInt(state);
			
			// Example of serializing an array:

			// It's a good idea to store the length explicitly
			int length = 0;

			if (neighbors != null) {
				length = neighbors.size();
			}

			// always write the length, since we need to know
			// even when it's zero
			out.writeInt(length);

			// now write each long in the array
			for (int i = 0; i < length; i++) {
				out.writeLong(neighbors.get(i).get());
			}
		}

		// Deserializes object - needed for Writable
		public void readFields(DataInput in) throws IOException {
			// example reading an int from the serialized object
			start = in.readLong();
			distance = in.readInt();
			state = in.readInt();			
			
			

			// example reading length from the serialized object
			int length = in.readInt();

			// Example of rebuilding the array from the serialized object
			neighbors = new ArrayList<LongWritable>();

			for (int i = 0; i < length; i++) {
				neighbors.add(new LongWritable(in.readLong()));
			}

		}

		public String toString() {
			// We highly recommend implementing this for easy testing and
			// debugging. This version just returns an empty string.
			StringBuffer s = new StringBuffer();
			s.append("[");
			s.append(this.start);
			s.append(", ");
			s.append(this.distance);
			s.append(", ");;
			if (this.state == 1) {
				s.append("Done, [ ");
			}
			else if (this.state == 0) {
				s.append("Not Done, [ ");
			}		
			else{
				s.append("Propogated, [ ");
				}
			for (LongWritable c : this.neighbors) {
				s.append(c.get());
				s.append(" ");
			}
			s.append("])]");
			return s.toString();
		}

		public int getDist() {
			return this.distance;
		}

		public int getState() {
			return this.state;
		}

		public ArrayList<LongWritable> getNeighbors() {
			return this.neighbors;
		}
		
		public long getStart() {
			return this.start;
		}
	}


	/*
	 * A toggleable print function. Change "print" variable to false to turn off
	 * all debugging print statements.
	 */
	public static void debugPrint(String s) {
		boolean print = false;
		if (print) {
			System.out.println(s);
		}
	}

	/*
	 * The first mapper. Part of the graph loading process, currently just an
	 * identity function. Modify as you wish.
	 */
	public static class LoaderMap extends
			Mapper<LongWritable, LongWritable, LongWritable, LongWritable> {

		@Override
		public void map(LongWritable key, LongWritable value, Context context)
				throws IOException, InterruptedException {

			SmallWorld.debugPrint("LoaderMap 1: (" + key.get() + " -> "
					+ value.get() + ")");

			context.write(key, value);

			SmallWorld.debugPrint("LoaderMap 2: (" + value.get() + " -- X)");
			context.write(value, new LongWritable(-1L));
		}
	}

	/*
	 * The first reducer. This is also currently an identity function (although
	 * it does break the input Iterable back into individual values). Modify it
	 * as you wish. In this reducer, you'll also find an example of loading and
	 * using the denom field.
	 */
	public static class LoaderReduce extends
			Reducer<LongWritable, LongWritable, LongWritable, Quintet> {

		public long denom;

		public void reduce(LongWritable key, Iterable<LongWritable> values,
				Context context) throws IOException, InterruptedException {
			// We can grab the denom field from context:
			denom = Long.parseLong(context.getConfiguration().get("denom"));

			double chance = 1.0 / ((double) (denom));
			double pick = Math.random();
			ArrayList<Long> vals = new ArrayList<Long>();
			ArrayList<LongWritable> valsLW = new ArrayList<LongWritable>();

			for (LongWritable v : values) {
				Long lw = v.get();
				if (lw >= 0L) {
					vals.add(lw);
				}
			}

			for (Long l : vals) {
				valsLW.add(new LongWritable(l));
			}

			Quintet value;

			// propogate
			value = new Quintet(key.get(), 0, -1, valsLW);

			SmallWorld.debugPrint("LoaderReduce 1: (" + key.get() + ", "
					+ value.toString() + ")");

			context.write(key, value);

			if (chance >= pick) {
				value = new Quintet(key.get(), 0, 1, valsLW);

				SmallWorld.debugPrint("LoaderReduce 2: (" + key.get() + ", "
						+ value.toString() + ")");

				context.write(key, value);
			}

		}

	}

	// ------- Add your additional Mappers and Reducers Here ------- //

	public static class BFSMap extends
			Mapper<LongWritable, Quintet, LongWritable, Quintet> {

		@Override
		public void map(LongWritable key, Quintet value, Context context)
				throws IOException, InterruptedException {

			if (value.getState() == -1) {
				// propogate

				SmallWorld.debugPrint("BFSMap 1: (" + key.get() + ", "
						+ value.toString());

				context.write(key, value);
			} else {
				if (value.getState() == 1) {

					SmallWorld.debugPrint("BFSMap 2: (" + key.get() + ", "
							+ value.toString());

					context.write(key, value);
				}
				for (LongWritable neighbor : value.getNeighbors()) {
					Quintet v;

					// handle cycles
					if (value.getStart() == neighbor.get()) {
						v = new Quintet(value.getStart(), 0, 1,
								new ArrayList<LongWritable>());
					} else {
						v = new Quintet(value.getStart(),
								(value.getDist() + 1), 1,
								new ArrayList<LongWritable>());
					}

					SmallWorld.debugPrint("BFSMap 3: (" + neighbor.get() + ", "
							+ v.toString());

					context.write(neighbor, v);
				}
			}
		}
	}

	public static class BFSReduce extends
			Reducer<LongWritable, Quintet, LongWritable, Quintet> {
		public void reduce(LongWritable key, Iterable<Quintet> values,
				Context context) throws IOException, InterruptedException {

			ArrayList<LongWritable> neighbors = new ArrayList<LongWritable>();
			HashSet<String> seen = new HashSet<String>();
			HashSet<Long> starts = new HashSet<Long>();
			HashMap<Long, Integer> distances = new HashMap<Long, Integer>();
			int it = Integer.parseInt(context.getConfiguration().get("iteration"));
			boolean propogated = false;
			
			SmallWorld.debugPrint("========== Iteration " + it + " ==========");
			
			if (it != MAX_ITERATIONS - 1) {

				for (Quintet q : values) {
	
					if (q.getState() == -1 && !propogated) {
						// propogate
	
						neighbors = q.getNeighbors();
						
						propogated = true;
	
						SmallWorld.debugPrint("BFSReduce 1: (" + key.get() + ", "
								+ q.toString());
	
						context.write(key, q);
						
					} else {
	
						String sg = "" + q.getStart();
	
						if (!seen.contains(sg)
								&& q.getState() == 1) {
							SmallWorld.debugPrint("BFSReduce 2: (" + key.get()
									+ ", " + q.toString());
							context.write(key, q);
							seen.add(sg);
						}
	
						long st = q.getStart();
						int dist = q.getDist();
						ArrayList<LongWritable> nei = q.getNeighbors();
	
						starts.add(st);
	
						if (distances.get(st) == null) {
							distances.put(st, dist);
						} else {
							int m = Math.min(dist, distances.get(st));
							distances.put(st, m);
						}
	
						if (neighbors.isEmpty()) {
							neighbors = nei;
						}
					}
				}
	
				Iterator iter = starts.iterator();
	
				while (iter.hasNext()) {
					long strt = (long) (Long) iter.next();
					Quintet value = new Quintet(strt, distances.get(strt), 0,
							neighbors);
	
					SmallWorld.debugPrint("BFSReduce 3: (" + key.get() + ", "
							+ value.toString());
	
					context.write(key, value);
				}
			}
			
			else{
				
				for (Quintet q : values) {
				
					if (q.getState() != -1) {
						String sg = "" + q.getStart();
		
						long st = q.getStart();
						int dist = q.getDist();
						ArrayList<LongWritable> nei = q.getNeighbors();
		
						starts.add(st);
		
						if (distances.get(st) == null) {
							distances.put(st, dist);
						} else {
							int m = Math.min(dist, distances.get(st));
							distances.put(st, m);
						}
		
						if (neighbors.isEmpty()) {
							neighbors = nei;
						}
					}
				}
	
				Iterator iter = starts.iterator();
		
				while (iter.hasNext()) {
					long strt = (long) (Long) iter.next();
					Quintet value = new Quintet(strt, distances.get(strt), 1,
							neighbors);
		
					SmallWorld.debugPrint("BFSReduce 3: (" + key.get() + ", "
							+ value.toString());
		
					context.write(key, value);
				}
				
				
			}
		}
	}

	public static class HistMap extends
			Mapper<LongWritable, Quintet, LongWritable, LongWritable> {

		public void map(LongWritable key, Quintet value, Context context)
				throws IOException, InterruptedException {

			if (value.getState() == 1) {
				long distance = (long) (value.getDist());
				LongWritable v = new LongWritable(1L);
				LongWritable k = new LongWritable(distance);

				SmallWorld.debugPrint("HistMap (" + k.get() + ", " + v.get()
						+ ")");

				context.write(k, v);
			}
		}
	}

	public static class HistReduce extends
			Reducer<LongWritable, LongWritable, LongWritable, LongWritable> {

		public void reduce(LongWritable key, Iterable<LongWritable> values,
				Context context) throws IOException, InterruptedException {

			long sum = 0L;
			for (LongWritable value : values) {
				sum += value.get();
			}
			SmallWorld
					.debugPrint("HistReduce (" + key.get() + ", " + sum + ")");
			context.write(key, new LongWritable(sum));
		}
	}

	public static void main(String[] rawArgs) throws Exception {
		GenericOptionsParser parser = new GenericOptionsParser(rawArgs);
		Configuration conf = parser.getConfiguration();
		String[] args = parser.getRemainingArgs();

		// Pass in denom command line arg:
		conf.set("denom", args[2]);

		// Sample of passing value from main into Mappers/Reducers using
		// conf. You might want to use something like this in the BFS phase:
		// See LoaderMap for an example of how to access this value
		conf.set("inputValue", (new Integer(5)).toString());

		// Setting up mapreduce job to load in graph
		Job job = new Job(conf, "load graph");
		
		job.setJarByClass(SmallWorld.class);
		
		job.setNumReduceTasks(24);

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Quintet.class);

		job.setMapperClass(LoaderMap.class);
		job.setReducerClass(LoaderReduce.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		// Input from command-line argument, output to predictable place
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path("bfs-0-out"));

		// Actually starts job, and waits for it to finish
		job.waitForCompletion(true);

		// Repeats your BFS mapreduce
		int i = 0;
		while (i < MAX_ITERATIONS) {
			conf.set("iteration", (new Integer(i)).toString());
			job = new Job(conf, "bfs" + i);
			job.setJarByClass(SmallWorld.class);
			
			job.setNumReduceTasks(24);

			// Feel free to modify these four lines as necessary:
			job.setMapOutputKeyClass(LongWritable.class);
			job.setMapOutputValueClass(Quintet.class);
			job.setOutputKeyClass(LongWritable.class);
			job.setOutputValueClass(Quintet.class);

			// You'll want to modify the following based on what you call
			// your mapper and reducer classes for the BFS phase.
			job.setMapperClass(BFSMap.class); // currently the default Mapper
			job.setReducerClass(BFSReduce.class); // currently the default
													// Reducer

			job.setInputFormatClass(SequenceFileInputFormat.class);
			job.setOutputFormatClass(SequenceFileOutputFormat.class);

			// Notice how each mapreduce job gets gets its own output dir
			FileInputFormat.addInputPath(job, new Path("bfs-" + i + "-out"));
			FileOutputFormat.setOutputPath(job, new Path("bfs-" + (i + 1)
					+ "-out"));

			job.waitForCompletion(true);
			i++;
		}

		// Mapreduce config for histogram computation
		job = new Job(conf, "hist");
		job.setJarByClass(SmallWorld.class);
		
		job.setNumReduceTasks(1);

		// Feel free to modify these two lines as necessary:
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(LongWritable.class);

		// DO NOT MODIFY THE FOLLOWING TWO LINES OF CODE:
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(LongWritable.class);

		// You'll want to modify the following based on what you call your
		// mapper and reducer classes for the Histogram Phase
		job.setMapperClass(HistMap.class); // currently the default Mapper
		job.setReducerClass(HistReduce.class); // currently the default Reducer

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		// By declaring i above outside of loop conditions, can use it
		// here to get last bfs output to be input to histogram
		FileInputFormat.addInputPath(job, new Path("bfs-" + i + "-out"));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
	}
}
