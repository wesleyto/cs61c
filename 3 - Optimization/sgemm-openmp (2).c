#include <nmmintrin.h>
#include <omp.h>
/*Jonathan Ma cs61c-dq
Wesley To cs61c-cv
*/
void sgemm( int m, int n, int d, float *A, float *C )
{
	int j, k, i, jn, jn1, kn;
	__m128 c0, c1, c2, c3, a0, a1, a2, a3, constant = _mm_setzero_ps();
	int n16 = n/16*16;
	int n8 = n/8*8;
	int n4 = n/4*4;
	#pragma omp parallel for private (j, jn, jn1, k, kn, constant, i)
    for(j = 0; j < n; j++ ){
		jn = j*n;
		jn1 = j*(n+1);
        for(k = 0; k < m; k++ ){
			kn = k*n;
			constant = _mm_load1_ps(A + j*(n+1) + kn);
			{
				for(i = 0; i < n16; i+=16 ){
					c0 = _mm_loadu_ps(C + i + jn);
					c1 = _mm_loadu_ps(C + i + 4 + jn);
					c2 = _mm_loadu_ps(C + i + 8 + jn);
					c3 = _mm_loadu_ps(C + i + 12 + jn);
					
					a0 = _mm_loadu_ps(A + i + kn);
					a1 = _mm_loadu_ps(A + i + kn + 4);
					a2 = _mm_loadu_ps(A + i + kn + 8);
					a3 = _mm_loadu_ps(A + i + kn + 12);
					
					c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
					c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
					c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
					c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
					
					_mm_storeu_ps(C + i + jn, c0);
					_mm_storeu_ps(C + i + jn + 4, c1);
					_mm_storeu_ps(C + i + jn + 8, c2);
					_mm_storeu_ps(C + i + jn + 12, c3);
				}
				for(i = n16; i < n8; i+=8 ){
					c0 = _mm_loadu_ps(C + i + jn);
					c1 = _mm_loadu_ps(C + i + 4 + jn);
					
					a0 = _mm_loadu_ps(A + i + kn);
					a1 = _mm_loadu_ps(A + i + kn + 4);
					
					c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
					c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
					
					_mm_storeu_ps(C + i + jn, c0);
					_mm_storeu_ps(C + i + jn + 4, c1);
				}
				for(i = n8; i < n4; i+=4 ){
					c0 = _mm_loadu_ps(C + i + jn);
					
					a0 = _mm_loadu_ps(A + i + kn);

					c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
					
					_mm_storeu_ps(C + i + jn, c0);
				}
				for(i = n4; i < n; i++ ) {
					C[i+jn] += A[i+kn] * A[jn1+kn];
				}
			}
		}
	}	
}