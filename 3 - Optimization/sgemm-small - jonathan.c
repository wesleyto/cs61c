#include <emmintrin.h>
/*Jonathan Ma cs61c-dq
Wesley To cs61c-cv
*/
void sgemm( int m, int n, int d, float *A, float *C )
{
    __m128 c0 = _mm_setzero_ps();
    __m128 c1 = _mm_setzero_ps();
    __m128 c2 = _mm_setzero_ps();
    __m128 c3 = _mm_setzero_ps();
    __m128 c4 = _mm_setzero_ps();
 
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();
    __m128 a2 = _mm_setzero_ps();
    __m128 a3 = _mm_setzero_ps();
    __m128 a4 = _mm_setzero_ps();
 
    __m128 constant = _mm_setzero_ps();
    int loop = 0;
    if(n == 40 && m == 40){
		int j, k, i, s = 0;
		#pragma omp parallel for private(j, k, i, s)
		for(j = 0; j < n; j++){
			int jn = j*n;
			for(k = 0; k < m; k+=4){
				for(i = 0; i < n; i += 20){
					//C[i+j*n] += A[i+k*(n)] * A[j*(n+1)+k*(n)];
					c0 = _mm_loadu_ps(C + i + jn);
					c1 = _mm_loadu_ps(C + i + jn + 4);
					c2 = _mm_loadu_ps(C + i + jn + 8);
					c3 = _mm_loadu_ps(C + i + jn + 12);
					c4 = _mm_loadu_ps(C + i + jn + 16);
					for (s = 0; s < 4; s++){
						constant = _mm_load1_ps(A + j*(n+1)+(k+s)*n);
					
						a0 = _mm_loadu_ps(A + i + (k+s)*n);
						a1 = _mm_loadu_ps(A + i + (k+s)*n + 4);
						a2 = _mm_loadu_ps(A + i + (k+s)*n + 8);
						a3 = _mm_loadu_ps(A + i + (k+s)*n + 12);
						a4 = _mm_loadu_ps(A + i + (k+s)*n + 16);
					
						c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
						c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
						c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
						c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
						c4 = _mm_add_ps(c4, _mm_mul_ps(a4, constant));
					}
					_mm_storeu_ps(C + i + jn, c0);
					_mm_storeu_ps(C + i + jn + 4, c1);
					_mm_storeu_ps(C + i + jn + 8, c2);
					_mm_storeu_ps(C + i + jn + 12, c3);
					_mm_storeu_ps(C + i + jn + 16, c4);	
				}
			}
		}
    }
    
    else if( n % 16 < 4){
		loop = n/16 * 16;
		int j, k, i = 0;
		#pragma omp parallel for private(j, k, i)
		for (j = 0; j < n; j++) {
			int jn = j*n;
			for (k = 0; k < m; k++) {
				int kn = k*n;
				constant = _mm_load1_ps(A + j*(n+1) + kn);
				for(i = 0; i < loop; i += 16){
					//C[i+j*n] += A[i+k*(n)] * A[j*(n+1)+k*(n)];
					c0 = _mm_loadu_ps(C + i + jn);
					c1 = _mm_loadu_ps(C + i + jn + 4);
					c2 = _mm_loadu_ps(C + i + jn + 8);
					c3 = _mm_loadu_ps(C + i + jn + 12);
				
					a0 = _mm_loadu_ps(A + i + kn);
					a1 = _mm_loadu_ps(A + i + kn + 4);
					a2 = _mm_loadu_ps(A + i + kn + 8);
					a3 = _mm_loadu_ps(A + i + kn + 12);
					
					c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
					c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
					c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
					c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
						
					_mm_storeu_ps(C + i + jn, c0);
					_mm_storeu_ps(C + i + jn + 4, c1);
					_mm_storeu_ps(C + i + jn + 8, c2);
					_mm_storeu_ps(C + i + jn + 12, c3);
				}
				for (int m = loop; i < n; i++) { 
					*(C + m + jn) += *(A + m + kn) * *(A + j*(n + 1) + kn);
				}
			}
		}
    }
    else// if (n % 12 < 3)
	{
		loop = n/12 * 12;
		int j, k, i = 0;
		#pragma omp parallel for private(j, k, i)
		for (j = 0; j < n; j++) {
			int jn = j*n;
			for (k = 0; k < m; k++) {
				int kn = k*n;
				constant = _mm_load1_ps(A + j*(n+1) + kn);
				for(i = 0; i < loop; i += 12){
					//C[i+j*n] += A[i+k*(n)] * A[j*(n+1)+k*(n)];
					c0 = _mm_loadu_ps(C + i + jn);
					c1 = _mm_loadu_ps(C + i + jn + 4);
					c2 = _mm_loadu_ps(C + i + jn + 8);
			
					a0 = _mm_loadu_ps(A + i + kn);
					a1 = _mm_loadu_ps(A + i + kn + 4);
					a2 = _mm_loadu_ps(A + i + kn + 8);
				
					c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
					c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
					c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
					
					_mm_storeu_ps(C + i + jn, c0);
					_mm_storeu_ps(C + i + jn + 4, c1);
					_mm_storeu_ps(C + i + jn + 8, c2);
				}
				for (int m = loop; m < n; i++) { 
					*(C + m + jn) += *(A + m + kn) * *(A + j*(n + 1) + kn);
				}
			}
		}
    }
    /* 
    else {
	loop = m/4 * 4;
	for (int j = 0; j < n; j++) {
	    int jn = j*n;
	    for (int k = 0; k < m; k++) {
		int kn = k*n;
		constant = _mm_load1_ps(A + j*(n+1) + kn);
		for(int i = 0; i < loop; i += 4){
		    C[i+j*n] += A[i+k*(n)] * A[j*(n+1)+k*(n)];
		    c0 = _mm_loadu_ps(C + i + jn);
		    c1 = _mm_loadu_ps(C + i + jn + 4);
	
		    a0 = _mm_loadu_ps(A + i + kn);
		    a1 = _mm_loadu_ps(A + i + kn + 4);
	    
		    c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
		    c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
		    
		    _mm_storeu_ps(C + i + jn, c0);
		    _mm_storeu_ps(C + i + jn + 4, c1);
		}
		for (int i = loop; i < n; i++) { 
		    *(C + i + jn) += *(A + i + kn) * *(A + j*(n + 1) + kn);
		}
	    }
	}
    }
    */
}
