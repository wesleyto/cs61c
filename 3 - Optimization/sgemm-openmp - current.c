
#include <emmintrin.h>
void sgemm(int m, int n, int d, float *A, float *C){
    int loop = n/32 * 32;
	// int nFoured = n/4*4;
	// int mFoured = m/4*4;
	#pragma omp parallel for
    for(int j = 0; j < n; j++){
		int jn = j*n;
		for(int i = 0; i < loop; i += 32){
			//C[i+j*n] += A[i+k*(n)] * A[j*(n+1)+k*(n)];
			__m128 c0 = _mm_loadu_ps(C + i + jn);
			__m128 c1 = _mm_loadu_ps(C + i + jn + 4);
			__m128 c2 = _mm_loadu_ps(C + i + jn + 8);
			__m128 c3 = _mm_loadu_ps(C + i + jn + 12);
			__m128 c4 = _mm_loadu_ps(C + i + jn + 16);
			__m128 c5 = _mm_loadu_ps(C + i + jn + 20);
			__m128 c6 = _mm_loadu_ps(C + i + jn + 24);
			__m128 c7 = _mm_loadu_ps(C + i + jn + 28);
			
			__m128 constant;

			__m128 a0, a1, a2, a3, a4, a5, a6, a7;

			for(int k = 0; k < m; k++){
				constant = _mm_load1_ps(A + j*(n+1) + k*n);
				a0 = _mm_loadu_ps(A + i + k*n);
				a1 = _mm_loadu_ps(A + i + k*n + 4);
				a2 = _mm_loadu_ps(A + i + k*n + 8);
				a3 = _mm_loadu_ps(A + i + k*n + 12);
				a4 = _mm_loadu_ps(A + i + k*n + 16);
				a5 = _mm_loadu_ps(A + i + k*n + 20);
				a6 = _mm_loadu_ps(A + i + k*n + 24);
				a7 = _mm_loadu_ps(A + i + k*n + 28);

				c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
				c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
				c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
				c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
				c4 = _mm_add_ps(c4, _mm_mul_ps(a4, constant));
				c5 = _mm_add_ps(c5, _mm_mul_ps(a5, constant));
				c6 = _mm_add_ps(c6, _mm_mul_ps(a6, constant));
				c7 = _mm_add_ps(c7, _mm_mul_ps(a7, constant));
			}
			_mm_storeu_ps(C + i + jn, c0);
			_mm_storeu_ps(C + i + jn + 4, c1);
			_mm_storeu_ps(C + i + jn + 8, c2);
			_mm_storeu_ps(C + i + jn + 12, c3);
			_mm_storeu_ps(C + i + jn + 16, c4);
			_mm_storeu_ps(C + i + jn + 20, c5);
			_mm_storeu_ps(C + i + jn + 24, c6);
			_mm_storeu_ps(C + i + jn + 28, c7);
			
			
		}
		// for(int i = loop; i < nFoured; i+=4){
			// for(int k = 0; k < mFoured; k+=4){
				// int kn = k*n;
				// int kn1 = (k+1)*n;
				// int kn2 = (k+2)*n;
				// int kn3 = (k+3)*n;
				// int i1 = i+1;
				// int i2 = i+2;
				// int i3 = i+3;
				// int jn1 = j*(n+1);
				// *(C + i + jn) += *(A + i + kn) * *(A + jn1 + kn);
				// *(C + i + jn) += *(A + i + kn1) * *(A + jn1 + kn1);
				// *(C + i + jn) += *(A + i + kn2) * *(A + jn1 + kn2);
				// *(C + i + jn) += *(A + i + kn3) * *(A + jn1 + kn3);
				
				// *(C + i1 + jn) += *(A + i1 + kn) * *(A + jn1 + kn);
				// *(C + i1 + jn) += *(A + i1 + kn1) * *(A + jn1 + kn1);
				// *(C + i1 + jn) += *(A + i1 + kn2) * *(A + jn1 + kn2);
				// *(C + i1 + jn) += *(A + i1 + kn3) * *(A + jn1 + kn3);
				
				// *(C + i2 + jn) += *(A + i2 + kn) * *(A + jn1 + kn);
				// *(C + i2 + jn) += *(A + i2 + kn1) * *(A + jn1 + kn1);
				// *(C + i2 + jn) += *(A + i2 + kn2) * *(A + jn1 + kn2);
				// *(C + i2 + jn) += *(A + i2 + kn3) * *(A + jn1 + kn3);
				
				// *(C + i3 + jn) += *(A + i3 + kn) * *(A + jn1 + kn);
				// *(C + i3 + jn) += *(A + i3 + kn1) * *(A + jn1 + kn1);
				// *(C + i3 + jn) += *(A + i3 + kn2) * *(A + jn1 + kn2);
				// *(C + i3 + jn) += *(A + i3 + kn3) * *(A + jn1 + kn3);
			// }
		// }
		for(int i = loop; i < n; i++){
			for(int k = 0; k < m; k++){
				*(C + i + jn) += *(A + i + k*n) * *(A + j*(n + 1) + k*n);
			}
		}
    }
}
/*void sgemm(int m, int n, int d, float *A, float *C){
    int loop = n/20 * 20;

    for(int j = 0; j < n; j++){
	int jn = j*n;
	for(int i = 0; i < loop; i += 20){
	    __m128 c0 = _mm_loadu_ps(C + i + jn);
	    __m128 c1 = _mm_loadu_ps(C + i + jn + 4);
	    __m128 c2 = _mm_loadu_ps(C + i + jn + 8);
	    __m128 c3 = _mm_loadu_ps(C + i + jn + 12);
	    __m128 c4 = _mm_loadu_ps(C + i + jn + 16);
	    __m128 constant;

	    __m128 a0;
	    __m128 a1;
	    __m128 a2;
	    __m128 a3;
	    __m128 a4;
	    for(int k = 0; k < m; k++){
		constant = _mm_load1_ps(A + j*(n+1) + k*n);
		a0 = _mm_loadu_ps(A + i + jn);
		a1 = _mm_loadu_ps(A + i + jn + 4);
		a2 = _mm_loadu_ps(A + i + jn + 8);
		a3 = _mm_loadu_ps(A + i + jn + 12);
		a4 = _mm_loadu_ps(A + i + jn + 16);

		c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
		c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
		c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
		c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
		c4 = _mm_add_ps(c4, _mm_mul_ps(a4, constant));

		_mm_storeu_ps(C + i + jn, c0);
		_mm_storeu_ps(C + i + jn + 4, c1);
		_mm_storeu_ps(C + i + jn + 8, c2);
		_mm_storeu_ps(C + i + jn + 12, c3);
		_mm_storeu_ps(C + i + jn + 16, c4);
		
	    }
	    for(int i = loop; i < n; i++){
		for(int k = 0; k < m; k++){
		    *(C + i + jn) += *(A + i + kn) * *(A + j*(n + 1) + kn);
		}
	    }
	}
    }
}
*/		    
		