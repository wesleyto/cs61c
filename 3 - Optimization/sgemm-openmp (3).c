

#include <emmintrin.h>
void sgemm(int m, int n, int d, float *A, float *C){
    int loop = n/20 * 20;
#pragma omp parallel for
    for(int j = 0; j < n; j++){
	int jn = j*n;
	for(int i = 0; i < loop; i += 20){
	    //C[i+j*n] += A[i+k*(n)] * A[j*(n+1)+k*(n)];
	    __m128 c0 = _mm_loadu_ps(C + i + jn);
	    __m128 c1 = _mm_loadu_ps(C + i + jn + 4);
	    __m128 c2 = _mm_loadu_ps(C + i + jn + 8);
	    __m128 c3 = _mm_loadu_ps(C + i + jn + 12);
	    __m128 c4 = _mm_loadu_ps(C + i + jn + 16);
	    __m128 constant;

	    __m128 a0;
	    __m128 a1;
	    __m128 a2;
	    __m128 a3;
	    __m128 a4;
	    for(int k = 0; k < m; k++){
		constant = _mm_load1_ps(A + j*(n+1) + k*n);
		a0 = _mm_loadu_ps(A + i + k*n);
		a1 = _mm_loadu_ps(A + i + k*n + 4);
		a2 = _mm_loadu_ps(A + i + k*n + 8);
		a3 = _mm_loadu_ps(A + i + k*n + 12);
		a4 = _mm_loadu_ps(A + i + k*n + 16);

		c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
		c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
		c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
		c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
		c4 = _mm_add_ps(c4, _mm_mul_ps(a4, constant));
	    }
		_mm_storeu_ps(C + i + jn, c0);
		_mm_storeu_ps(C + i + jn + 4, c1);
		_mm_storeu_ps(C + i + jn + 8, c2);
		_mm_storeu_ps(C + i + jn + 12, c3);
		_mm_storeu_ps(C + i + jn + 16, c4);
		
	    
	}
	    for(int i = loop; i < n; i++){
		for(int k = 0; k < m; k++){
		    *(C + i + jn) += *(A + i + k*n) * *(A + j*(n + 1) + k*n);
		}
	    }
    }
}
/*void sgemm(int m, int n, int d, float *A, float *C){
    int loop = n/20 * 20;

    for(int j = 0; j < n; j++){
	int jn = j*n;
	for(int i = 0; i < loop; i += 20){
	    __m128 c0 = _mm_loadu_ps(C + i + jn);
	    __m128 c1 = _mm_loadu_ps(C + i + jn + 4);
	    __m128 c2 = _mm_loadu_ps(C + i + jn + 8);
	    __m128 c3 = _mm_loadu_ps(C + i + jn + 12);
	    __m128 c4 = _mm_loadu_ps(C + i + jn + 16);
	    __m128 constant;

	    __m128 a0;
	    __m128 a1;
	    __m128 a2;
	    __m128 a3;
	    __m128 a4;
	    for(int k = 0; k < m; k++){
		constant = _mm_load1_ps(A + j*(n+1) + k*n);
		a0 = _mm_loadu_ps(A + i + jn);
		a1 = _mm_loadu_ps(A + i + jn + 4);
		a2 = _mm_loadu_ps(A + i + jn + 8);
		a3 = _mm_loadu_ps(A + i + jn + 12);
		a4 = _mm_loadu_ps(A + i + jn + 16);

		c0 = _mm_add_ps(c0, _mm_mul_ps(a0, constant));
		c1 = _mm_add_ps(c1, _mm_mul_ps(a1, constant));
		c2 = _mm_add_ps(c2, _mm_mul_ps(a2, constant));
		c3 = _mm_add_ps(c3, _mm_mul_ps(a3, constant));
		c4 = _mm_add_ps(c4, _mm_mul_ps(a4, constant));

		_mm_storeu_ps(C + i + jn, c0);
		_mm_storeu_ps(C + i + jn + 4, c1);
		_mm_storeu_ps(C + i + jn + 8, c2);
		_mm_storeu_ps(C + i + jn + 12, c3);
		_mm_storeu_ps(C + i + jn + 16, c4);
		
	    }
	    for(int i = loop; i < n; i++){
		for(int k = 0; k < m; k++){
		    *(C + i + jn) += *(A + i + kn) * *(A + j*(n + 1) + kn);
		}
	    }
	}
    }
}
*/		    
		